function printpdf() {
    $.getScript("src/js/jspdf.min.js")
        .done(function (script, textStatus) {
            if (textStatus === 'success') {
                var doc = new jsPDF();
                var specialElementHandlers = {
                    '#editor': function (element, renderer) {
                        return true;
                    }
                };

                $('#alert_warning').slideDown("slow", function () {
                    $('#alert_warning').css('height', '52px');
                    $('#alert_warning').css('display', 'block');
                });

                $('#alert_warning').html('<strong>Warning!</strong> The actual formating of home will be altered.');

                $('#pdf-download').click(function () {

                    doc.fromHTML(window.document.getElementById("actual_resume"), 15, 15, {
                        'width': 180,
                        'elementHandlers': specialElementHandlers
                    });

                    doc.save('narender-singh-home.pdf');
                });
            }
        })
        .fail(function (jqxhr, settings, exception) {
            alert('Error connecting to source. [' + exception + ']');
        });
}