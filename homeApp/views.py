from django.shortcuts import render, HttpResponse
from .models import ContactForm
from django.core.validators import validate_email
from django.core.exceptions import ValidationError


# Basic response from index
def index(request):
    url_path = request.path
    if url_path == '/contact':
        name = request.GET['name']
        email = request.GET['email']
        message = request.GET['message']
        if name == '' or name is None:
            return HttpResponse(request, 'Please enter valid name')
        elif email == '' or email is None:
            return HttpResponse(request, 'Please enter valid email')
        elif message == '' or message is None:
            return HttpResponse(request, 'Don\'t you wanna add some message for me to read')
        else:
            try:
                validate_email(email)
                contact_form = ContactForm(name=name, email=email, message=message)
                contact_form.save()
                return render(request, template_name='home/thanks.html')
            except ValidationError:
                return HttpResponse('Invalid email Id detected, please drop me a mail on narender@unix.net.')
            except Exception:
                return HttpResponse('please drop me a mail on narender@unix.net.')

    else:
        return render(request, template_name='home/home.html')
