# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


# Create your models here.
class ContactForm(models.Model):
    name = models.CharField(max_length=64, null=False, blank=False)
    email = models.EmailField(max_length=64, null=False, blank=False)
    message = models.CharField(max_length=140, null=False, blank=False)

    def __str__(self):
        return self.name
