# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import ContactForm

admin.site.site_header = 'Pro.Narender Administration'
admin.site.site_title = 'Pro.Narender'


class ContactFormAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'message')


# Register your models here.
admin.site.register(ContactForm, ContactFormAdmin)
