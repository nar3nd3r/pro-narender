from django.shortcuts import render


def error_400(request):
    data = {}
    return render(request, 'html/HTTP400.html', data)


def error_403(request):
    data = {}
    return render(request, 'html/HTTP403.html', data)


def error_404(request):
    data = {}
    return render(request, 'html/HTTP404.html', data)


def error_500(request):
    data = {}
    return render(request, 'html/HTTP500.html', data)
