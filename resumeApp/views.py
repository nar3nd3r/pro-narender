from django.shortcuts import render
from django.http import HttpResponse


# Basic response from index
def index(request):
    context = dict()
    context['summary_top'] = """
    Award-winning python/django backend developer cum instructor with 8 years’ of well-rounded experience in MAMP/LAMP and extension development for Firefox/Chrome.
    An open source enthusiast, with forward-thinking and result oriented attitude.
    """
    context['summary_bottom'] = """
        Demonstrable experience in building, integrating, unit testing, and supporting web cum cloud applications in 
        Telecom, Storage and Health Care. I've passion for Amazon Web Services (AWS) and Data Science.
    """

    return render(request, template_name='resume/resume.html', context=context)
    # return HttpResponse('<h2>It works</h2>')
