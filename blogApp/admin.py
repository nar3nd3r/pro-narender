from django.contrib import admin
from .models import Post


class PostAdmin(admin.ModelAdmin):
    list_display = ('post_name', 'post_date', 'post_content_desc', 'post_tags')

    # we need only first 80 character from the post_content, so to called it post_content_desc
    def post_content_desc(self, obj):
        return obj.post_content[:80] + '...'

    # Small name for Post content instead of post_content_desc
    post_content_desc.short_description = 'Post Content'


# Register your models here.
admin.site.register(Post, PostAdmin)
