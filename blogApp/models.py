from datetime import datetime

from django.db import models


# Let's create the post model here.
class Post(models.Model):
    mood_list = (
        ('Normal', 'Blank Face'),
        ('Happy', 'Happy Face'),
        ('Tensed', 'Tensed and Low'),
        ('Funny', 'Being Sarcastic'),
        ('Glasses', 'Being Nerdy'),
        ('Holy Crap', 'What the heck!')
    )

    # author_mood: str = models.CharField(max_length=10, null=False, blank=False, default='Normal', choices=mood_list)
    # post_name: str = models.CharField(max_length=64, null=False, blank=False)
    # post_date: datetime = models.DateTimeField(auto_now=True)
    # post_content: str = models.TextField(max_length=3000, null=False, blank=False, default='What did you do today.')
    # post_tags: str = models.CharField(max_length=64, null=False, blank=False, default='blog')
    author_mood = models.CharField(max_length=10, null=False, blank=False, default='Normal', choices=mood_list)
    post_name = models.CharField(max_length=64, null=False, blank=False)
    post_date = models.DateTimeField(auto_now=True)
    post_content = models.TextField(max_length=3000, null=False, blank=False, default='What did you do today.')
    post_tags = models.CharField(max_length=64, null=False, blank=False, default='blog')

    def __str__(self):
        return self.post_name
