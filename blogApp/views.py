from django.shortcuts import render
from .models import Post


# Basic response from index
def index(request):
    return render(request, template_name='blog/blog.html')


def get_blog(request):
    post_url = ' '.join(request.path.split('/')[2].split('-'))
    try:
        post = Post.objects.get(post_name__iexact=post_url)
        context = dict()
        if post:
            if post.author_mood == 'Normal':
                context['author_mood_url'] = 'img/blank_face.svg'
            elif post.author_mood == 'Happy':
                context['author_mood_url'] = 'img/happy_face.svg'
            elif post.author_mood == 'Tensed':
                context['author_mood_url'] = 'img/heisenberg.svg'
            elif post.author_mood == 'Glasses':
                context['author_mood_url'] = 'img/being_nerdy.svg'
            elif post.author_mood == 'Glasses':
                context['author_mood_url'] = 'img/being_sarcastic.svg'
            else:
                context['author_mood_url'] = 'img/what_the_heck.svg'

            context['author_mood'] = post.author_mood
            context['post_name'] = post.post_name
            context['post_date'] = post.post_date
            context['post_content'] = post.post_content
            context['post_tags'] = post.post_tags
            return render(request, template_name='blog/blog.html', context=context)
    except Exception:
        return render(request, template_name='blog/404.html')
